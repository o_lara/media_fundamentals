# Media Fundamentals

Branches on this repository will be created as follows: 

* week_01
* week_02
* week_03
* ...

When working on class make sure to checkout corresponding branch, else you might not get current code. 
Current branch name will be provided in class. 

**To clone repository:**

_git clone git@bitbucket.org:o_lara/media_fundamentals.git_

**To view existing branches (local and remote):**

_git branch -a_

**To checkout specific branch:**

_git checkout yourbranchname_

**To get latest code from repository: **

_git pull_

Note: If you have local changes "git pull" might not get the latest changes. 

The following will override any local changes, then you can pull latest changes from remote repository:
* _git reset --hard_
* _git pull_
 
**If you have too many changes or do not know what to do, your best choise is to _git clone_(see above) in a new folder.**
 
**I will use git extensions for this project:**
_http://gitextensions.github.io_

_The code provided here is for academic purposes only._ 

