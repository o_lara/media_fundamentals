//===========================================================================================================================================================
//This software is intended for academic purposes only.
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
//WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//===========================================================================================================================================================

//===========================================================================================================================================================
//NOTES:

//Include for 64x OS: 
//C:\Program Files (x86)\FMOD SoundSystem\FMOD Studio API Windows\api\lowlevel\inc
//Include for 32x OS: 
//C:\Program Files\FMOD SoundSystem\FMOD Studio API Windows\api\lowlevel\inc

//Lib for 64x OS:  
//C:\Program Files (x86)\FMOD SoundSystem\FMOD Studio API Windows\api\lowlevel\lib
//Lib for 32x OS:  
//C:\Program Files\FMOD SoundSystem\FMOD Studio API Windows\api\lowlevel\lib

//Both files are under Lib folder above: 
//fmod_vc.lib
//fmod.dll
//
// You might need to copy fmod.dll to where your *.exe file is located in order to successfully run this project.
//===========================================================================================================================================================

#include <fmod.hpp>
#include <fmod_errors.h>
#include <iostream>
#include <conio.h> 
#include <Windows.h>
void ERRCHECK(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		printf("FMOD error %d - %s\n", result, FMOD_ErrorString(result));
		std::cin.get();
		exit(-1);
	}
	
}

LARGE_INTEGER frequency; //ticks per second
LARGE_INTEGER t1, t2; // ticks

void start_timer()
{

	//Start timer 1
	QueryPerformanceCounter(&t1);
}

double stop_timer()
{
	double elapsedTime;
	//Start timer 2
	QueryPerformanceCounter(&t2);

	// computer elapsed time in milliseconds
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;

	start_timer();

	return elapsedTime;
}

int main()
{
	QueryPerformanceFrequency(&frequency);

	FMOD::System     *system;
	FMOD::Sound      *sound1;
	FMOD::Sound      *sound2;
	FMOD::Channel    *channel = 0;
	FMOD_RESULT       result;
	unsigned int      version;

	//Create system
	result = FMOD::System_Create(&system);
	ERRCHECK(result);

	//Get version
	result = system->getVersion(&version);
	ERRCHECK(result);

	//Validate version
	if (version < FMOD_VERSION)
	{
		printf("FMOD lib version %08x doesn't match header version %08x", version, FMOD_VERSION);
		std::cin.get();
		exit(-1);
	}

	//Initialize FMOD.
	result = system->init(32, FMOD_INIT_NORMAL, NULL);
	ERRCHECK(result);

	//Load sound from file. 
	result = system->createSound("media/jaguar.wav", FMOD_DEFAULT, 0, &sound1);
	ERRCHECK(result);

	//Load sound from file.
	result = system->createSound("media/bump_of_chicken_helloworld.mp3", FMOD_DEFAULT, 0, &sound2);
	ERRCHECK(result);


	//Stores keyboard char input.
	int c = '.';

	double elapsedTimeInMilliseconds = 0.0;

	//Create main loop
	do{
		//Read keyboar only when key has been pressed.
		if (_kbhit())
		{
			//Read keyboard input char
			c = getchar();
		}

		//Validate keyboard input char and execute code accordingly.
		if (c == '1')
		{
			result = system->playSound(sound1, 0, false, &channel);
			ERRCHECK(result);
			c = '.';
		}
		else if (c == '2')
		{
			result = system->playSound(sound2, 0, false, &channel);
			ERRCHECK(result);
			c = '.';
		}

		FMOD::Sound *currentSound = 0;
		unsigned int lenms = 0;
		if (channel)
		{
			//Get current sound
			channel->getCurrentSound(&currentSound);
			if (currentSound)
			{
				result = currentSound->getLength(&lenms, FMOD_TIMEUNIT_MS);
				ERRCHECK(result);
			}

		}

		//Print some information on console
		elapsedTimeInMilliseconds += stop_timer();
		if (elapsedTimeInMilliseconds >= 4000)
		{
			//elapsedTimeInMilliseconds = 0.0;

			//Clear console
			std::system("cls");
			//Print somenthing
			printf("Press 0 to exit.\n");
			printf("Press 1 to play jaguar.\n");
			printf("Press 2 to play mp3.\n");	
			printf("Current song length in ms: %d\n", lenms);
			//We estimated it would be 262000 based on 4mins 22 sconds.
		}


	} while (c != '0'); //Exit if user press 0


	//Shutdown
	result = sound1->release();
	ERRCHECK(result);
	result = sound2->release();
	ERRCHECK(result);
	result = system->close();
	ERRCHECK(result);
	result = system->release();
	ERRCHECK(result);


	return 0;
}
